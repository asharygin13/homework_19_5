
#include <iostream>
#include <vector>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Animals " << "\n";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof! " << "\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow! " << "\n";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moo! " << "\n";
    }
};

int main()
{
    // �������� ��������� ���������� ��� �������� �������
    std::vector<Animal*> animals;
    animals.push_back(new Dog());
    animals.push_back(new Cat());
    animals.push_back(new Cow());

    for (Animal* animal : animals) 
    {
        animal->Voice();
    }

    return 0;
}
